from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

from src.config.config import DATABASE_URL

# Создание сессии
engine = create_engine(DATABASE_URL)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# Подключение базы (с автоматической генерацией моделей)
Base = declarative_base()


class City(Base):
    """
    Город
    """
    __tablename__ = 'city'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)

    picnics = relationship('Picnic', backref='city')

    def __repr__(self):
        return f'<Город "{self.name}">'


class User(Base):
    """
    Пользователь
    """
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    age = Column(Integer, nullable=True)

    def __repr__(self):
        return f'<Пользователь {self.surname} {self.name}>'


class Picnic(Base):
    """
    Пикник
    """
    __tablename__ = 'picnic'

    id = Column(Integer, primary_key=True, autoincrement=True)
    city_id = Column(Integer, ForeignKey('city.id'), nullable=False)
    time = Column(DateTime, nullable=False)

    def __repr__(self):
        return f'<Пикник {self.id}>'


# в текущих требованиях можно было бы использовать many to many из sqlalchemy,
# но предполагаю, что расширять такую таблицу будет как минимум неудобно
# association_table = Table('picnic_registration', Base.metadata,
#    Column('user_id', ForeignKey('user.id'), primary_key=True, nullable=False),
#    Column('picnic_id', ForeignKey('picnic.id'), primary_key=True, nullable=False)
# )
class PicnicRegistration(Base):
    """
    Регистрация пользователя на пикник
    """
    __tablename__ = 'picnic_registration'
    # стоит реализовать unique constraint, чтобы запретить дублирование на уровне БД,
    # но я не знаю, можно ли править модели бд в рамках тестового, поэтому проверка в сервисе
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    picnic_id = Column(Integer, ForeignKey('picnic.id'), nullable=False)

    user = relationship('User', backref='picnics')
    picnic = relationship('Picnic', backref='users')

    def __repr__(self):
        return f'<Регистрация {self.id}>'


# вот это выглядит неправильным, но я пока не понимаю, что с этим сделать
# ну и нехватает alembic для контроля миграций
Base.metadata.create_all(bind=engine)
