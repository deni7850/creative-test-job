from starlette.config import Config
from starlette.datastructures import Secret

config = Config('.env')
PROJECT_NAME = "picnic"


DATABASE_USER = config("DATABASE_USER", cast=str)
DATABASE_PASSWORD = config("DATABASE_PASSWORD", cast=Secret)
DATABASE_HOST = config("DATABASE_HOST", cast=str, default="postgres-picnic")
DATABASE_PORT = config("DATABASE_PORT", cast=str, default="5432")
DATABASE_NAME = config("DATABASE_NAME", cast=str)
DATABASE_URL = config(
  "DATABASE_URL",
  cast=str,
  default=f"postgresql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/{DATABASE_NAME}"
)
# можно использовать для пересоздания тестовой бд
# POSTGRES_DB_URL = config(
#   "DATABASE_URL",
#   cast=str,
#   default=f"postgresql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/postgres"
# )

WEATHER_API_KEY = '99ba78ee79a2a24bc507362c5288a81b'
