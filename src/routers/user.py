from fastapi import APIRouter, Depends

from src.models import Session
from src.routers import get_db
from src.schemas import UserModel, RegisterUserRequest, GetUsersRequest
from src.services.user import UserService

router = APIRouter(prefix='/user', tags=['user'])


@router.get('/get', summary='Get Users')
def users_list(age: GetUsersRequest = Depends(), db: Session = Depends(get_db)):
    """
    Список пользователей
    """
    service = UserService(db)
    if age.min_age is not None or age.max_age is not None:
        users = service.get_users_by_age(age.min_age, age.max_age)
    else:
        users = service.get_users()
    return users


@router.post('/create', summary='Create User', response_model=UserModel)
def register_user(user: RegisterUserRequest, db: Session = Depends(get_db)):
    """
    Регистрация пользователя
    """
    user = UserService(db).create_user(user)

    return user
