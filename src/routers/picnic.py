from fastapi import Depends, APIRouter, HTTPException

from src import schemas
from src.models import Session
from src.routers import get_db
from src.schemas import CreatePicnicRequest, CreatePicnicRegistrationRequest
from src.services.picnic import PicnicService
from src.utils.exceptions import ForeignKeyIntegrityError

router = APIRouter(prefix='/picnic', tags=['picnic'])


@router.get('/get', summary='All Picnics')
def all_picnics(picnics_filter: schemas.GetPicnicsRequest = Depends(), db: Session = Depends(get_db)):
    """
    Список всех пикников
    """
    picnics = PicnicService(db).get_picnics(picnics_filter)

    return picnics


@router.post('/create', summary='Picnic Add')
def picnic_add(picnic: CreatePicnicRequest = Depends(), db: Session = Depends(get_db)) -> schemas.PicnicResponse:
    """
    Создание пикника
    """
    try:
        picnic = PicnicService(db).create_picnic(picnic)
    except ForeignKeyIntegrityError as e:
        raise HTTPException(e.status_code, e.detail)
    return picnic


@router.post('/register', summary='Picnic Registration')
def register_to_picnic(registration: CreatePicnicRegistrationRequest, db: Session = Depends(get_db)):
    """
    Регистрация пользователя на пикник
    """
    # наверное стоит запрещать регистрацию на прошедшие пикники
    try:
        registration = PicnicService(db).register_to_picnic(registration)
    except ForeignKeyIntegrityError as e:
        raise HTTPException(e.status_code, e.detail)
    return registration
