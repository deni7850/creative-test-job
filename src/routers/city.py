from typing import List

from fastapi import APIRouter, Depends

from src import schemas
from src.models import Session
from src.routers import get_db
from src.services.city import CityService

router = APIRouter(prefix='/city', tags=['city'])


@router.post('/create', summary='Create City', description='Создание города по его названию')
def create_city(city: schemas.CreateCityRequest, db: Session = Depends(get_db)):
    city = CityService(db).create_city(city)

    return city


@router.get('/get', summary='Get Cities')
def cities_list(city: schemas.GetCitiesRequest = Depends(), db: Session = Depends(get_db)) -> List[schemas.City]:
    """
    Получение списка городов
    """
    city_service = CityService(db)
    if city.name:
        cities = city_service.get_city_by_name(city_name=city.name)
    else:
        cities = city_service.get_cities()

    return cities


@router.get('/get-weather', summary='Get City Weather')
def get_city_weather(city: schemas.CityBase = Depends(), db: Session = Depends(get_db)) -> List[schemas.CityWeather]:
    # решил вынести в отдельный метод, т.к. это позволит отделить поиск города от желания узнать погоду,
    # не будет лишних запросов к внешнему сервису
    cities = CityService(db).get_weather_for_city(city_name=city.name)
    return cities
