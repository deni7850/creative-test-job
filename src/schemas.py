import datetime as dt
from typing import Optional, List

from pydantic import BaseModel, Field


# todo: schemas.py-> папка с файлами пикник, пользователь, город
class RegisterUserRequest(BaseModel):
    name: str
    surname: str
    age: int


class GetUsersRequest(BaseModel):
    min_age: Optional[int]
    max_age: Optional[int]


class UserModel(BaseModel):
    id: int
    name: str
    surname: str
    age: int

    class Config:
        orm_mode = True


class CityBase(BaseModel):
    name: str = Field(description="Название города")


class GetCitiesRequest(CityBase):
    name: Optional[str] = Field(description="Название города", default=None)


class CreateCityRequest(CityBase):
    pass


class City(CityBase):
    id: int

    class Config:
        orm_mode = True


class CityWeather(CityBase):
    id: int
    weather: str


class PicnicBase(BaseModel):
    time: dt.datetime


class CreatePicnicRequest(PicnicBase):
    city_id: int


class Picnic(PicnicBase):
    id: int
    city: City
    users: List['PicnicRegistration'] = Field(default_factory=list)

    class Config:
        orm_mode = True


class PicnicResponse(PicnicBase):
    id: int
    city: str


class GetPicnicsRequest(BaseModel):
    # как-то неудобно фильтровать пикники по datetime, можно сделать промежуток
    # ну и искать пикники по городу
    datetime: dt.datetime = Field(default=None, description='Время пикника (по умолчанию не задано)')
    past: bool = Field(default=True, description='Включая уже прошедшие пикники')


class PicnicRegistrationBase(BaseModel):
    user_id: int
    picnic_id: int


class CreatePicnicRegistrationRequest(PicnicRegistrationBase):
    pass


class PicnicRegistration(PicnicRegistrationBase):
    id: int
    user: UserModel

    class Config:
        orm_mode = True


class PicnicRegistrationResponse(PicnicRegistrationBase):
    pass


class PicnicRegisteredUser(UserModel):
    class Config:
        orm_mode = False


class PicnicSummaryResponse(PicnicResponse):
    users: List[PicnicRegisteredUser]


Picnic.update_forward_refs()
