import requests
from pydantic import BaseModel, Field

from src.config.config import WEATHER_API_KEY


# по поводу проблем с масштабированием не могу говорить с полной уверенностью,
# но думаю, что стоит кешировать запросы к сервису погоды.


class GetWeatherParams(BaseModel):
    city: str = Field(description='Название города', alias='q')
    units: str = 'metric'
    appid: str = WEATHER_API_KEY


class WeatherDataMain(BaseModel):
    temp: float
    humidity: int


class GetWeatherResponse(BaseModel):
    main: WeatherDataMain


class WeatherService:
    def __init__(self):
        """
        Инициализирует класс
        """
        self.weather_api_url = 'https://api.openweathermap.org/data/2.5/weather'
        self.session = requests.Session()


class OpenWeatherApi(WeatherService):
    """
    Выполняет запрос на получение текущей погоды для города
    """

    def get_weather(self, params: GetWeatherParams) -> WeatherDataMain:
        response = self.session.get(url=self.weather_api_url, params=params.dict(by_alias=True))
        if response.status_code != 200:
            response.raise_for_status()
        data = response.json()
        return GetWeatherResponse.parse_obj(data).main

    def check_city_existing(self, params: GetWeatherParams) -> bool:
        response = self.session.get(url=self.weather_api_url, params=params.dict(by_alias=True))
        if response.status_code == 404:
            return False
        elif response.status_code == 200:
            return True
        else:
            response.raise_for_status()
