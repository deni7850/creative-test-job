# todo: описать ошибки сервиса
class AppException(Exception):
    def __init__(self, status_code: int, context: dict, detail: str):
        self.exception_case = self.__class__.__name__
        self.status_code = status_code
        self.context = context
        self.detail = detail


class ForeignKeyIntegrityError(AppException):
    def __init__(self, context: dict = None, detail: str = None):
        status_code = 400
        AppException.__init__(self, status_code, context, detail)
