from typing import List

from fastapi import HTTPException

from src import schemas
from src.services.main import AppService
from src.services.repository import CityRepository
from src.utils.external_requests import OpenWeatherApi, GetWeatherParams


class CityService(AppService):
    def create_city(self, city: schemas.CreateCityRequest) -> schemas.City:
        if city is None:
            raise HTTPException(status_code=400, detail='Параметр city должен быть указан')

        weather_client = OpenWeatherApi()
        if not weather_client.check_city_existing(params=GetWeatherParams(q=city.name)):
            raise HTTPException(status_code=400, detail='Параметр city должен быть существующим городом')

        city.name = city.name.capitalize()
        city = CityRepository.create_city(self.db, city)
        return city

    def get_city_by_name(self, city_name: str):
        return CityRepository.get_city_by_name(self.db, city_name)

    def get_cities(self, skip: int = 0, limit: int = 100) -> List[schemas.City]:
        return CityRepository.get_cities(self.db, skip, limit)

    def get_weather_for_city(self, city_name: str) -> List[schemas.CityWeather]:
        """
        Возвращает текущую погоду в найденных городах
        """
        # в property было обращение к внешнему сервису - логике место в сервисе
        cities = self.get_city_by_name(city_name)
        weather_client = OpenWeatherApi()
        cities = [
            schemas.CityWeather(
                weather=str(weather_client.get_weather(
                    params=GetWeatherParams(q=city.name)).temp),
                **city.dict()) for city in cities]

        return cities
