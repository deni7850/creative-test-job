from typing import List, Optional

from src import schemas
from src.services.main import AppService
from src.services.repository import UserRepository


class UserService(AppService):
    def create_user(self, user: schemas.RegisterUserRequest) -> schemas.UserModel:
        # недостает уникальности пользователя, можно насоздавать дублей
        return UserRepository.create_user(self.db, user)

    def get_users(self, skip: int = 0, limit: int = 100) -> List[schemas.UserModel]:
        return UserRepository.get_users(self.db, skip, limit)

    def get_users_by_age(self, min_age: Optional[int], max_age: Optional[int]
                         ) -> List[schemas.UserModel]:
        return UserRepository.get_users_by_age(self.db, min_age, max_age)
