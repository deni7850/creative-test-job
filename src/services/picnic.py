from typing import List

from src import schemas
from src.services.main import AppService
from src.services.repository import PicnicRepository, PicnicRegistrationRepository


class PicnicService(AppService):
    def get_picnics(self, picnics_filter: schemas.GetPicnicsRequest) -> List[schemas.PicnicSummaryResponse]:
        picnics = PicnicRepository(self.db).get_picnics(picnics_filter)
        picnics = [schemas.PicnicSummaryResponse(
            time=picnic.time,
            id=picnic.id,
            city=picnic.city.name,
            users=[
                schemas.PicnicRegisteredUser(
                    **schemas.UserModel.from_orm(registration.user).dict())
                for registration in picnic.users
            ]
        ) for picnic in picnics]
        return picnics

    def create_picnic(self, picnic: schemas.CreatePicnicRequest) -> schemas.PicnicResponse:
        picnic = PicnicRepository(self.db).create_picnic(picnic)
        picnic = schemas.PicnicResponse(
            id=picnic.id,
            city=picnic.city.name,
            time=picnic.time
        )
        return picnic

    def register_to_picnic(self, registration: schemas.CreatePicnicRegistrationRequest):
        registration = PicnicRegistrationRepository.get_or_create(self.db, registration)
        return schemas.PicnicRegistrationResponse(**registration.dict(exclude={'user'}))
