from sqlalchemy.orm import Session


class DBSession:
    def __init__(self, db: Session):
        self.db = db


class AppService(DBSession):
    pass


class AppRepository(DBSession):
    # по идее, стоит унаследоваться от этого класса,
    # не нужно будет в методы репозитория посылать параметр сессии,
    # и работать с бд через self.db
    # но я пока не знаю, лучшие практики разработки на fastapi
    # руководствовался статьей
    # https://camillovisini.com/article/abstracting-fastapi-services/
    pass
