from datetime import datetime
from typing import List, Optional

from sqlalchemy import and_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from src import models
from src import schemas
from src.services.main import AppRepository
from src.utils.exceptions import ForeignKeyIntegrityError


class CityRepository:

    @classmethod
    def create_city(cls, db: Session, city: schemas.CreateCityRequest) -> schemas.City:
        # Тюмень и Tyumen будут разными городами, стоит идентифицировать как-то иначе
        city_object = db.query(models.City).filter(models.City.name == city.name).first()
        if city_object is None:
            city_object = models.City(**city.dict())
            db.add(city_object)
            db.commit()
        return schemas.City.from_orm(city_object)

    @classmethod
    def get_cities(cls, db: Session, skip: int = 0, limit: int = 100) -> List[schemas.City]:
        cities = db.query(models.City).offset(skip).limit(limit).all()
        return [schemas.City.from_orm(city) for city in cities]

    @classmethod
    def get_city_by_name(cls, db: Session, query: str) -> List[schemas.City]:
        """ Получение списка городов по имени
        :param db: сессия БД
        :param query: Имя города или его часть
        :return: Список городов, удовлетворяющих условию
        """
        # case-insensitive не работает в sqlite + unicode
        # есть решение этой задачи https://habr.com/ru/post/57915/
        # перечитал условие задачи и понял, что нужно искать все города, а не один
        cities = db.query(models.City).filter(models.City.name.ilike(query)).all()
        return [schemas.City.from_orm(city) for city in cities]


class UserRepository:
    @classmethod
    def create_user(cls, db: Session, user: schemas.RegisterUserRequest) -> schemas.UserModel:
        user_object = models.User(**user.dict())
        db.add(user_object)
        db.commit()
        return schemas.UserModel.from_orm(user_object)

    @classmethod
    def get_users(cls, db: Session, skip: int = 0, limit: int = 100) -> List[schemas.UserModel]:
        users = db.query(models.User).offset(skip).limit(limit).all()
        return [schemas.UserModel.from_orm(user) for user in users]

    @classmethod
    def get_users_by_age(cls, db: Session,
                         min_age: Optional[int], max_age: Optional[int]
                         ) -> List[schemas.UserModel]:
        users = db.query(models.User)
        if min_age:
            users = users.filter(models.User.age >= min_age)
        if max_age:
            users = users.filter(models.User.age <= max_age)

        users = users.all()
        return [schemas.UserModel.from_orm(user) for user in users]


class PicnicRegistrationRepository:
    @classmethod
    def get_or_create(cls, db: Session, registration_request: schemas.CreatePicnicRegistrationRequest
                      ) -> schemas.PicnicRegistration:
        # можно реализовать проверку через UniqueConstraint и обработку исключения,
        # но в таком случае часть логики ляжет на БД
        registration = db.query(models.PicnicRegistration) \
            .filter(and_(models.PicnicRegistration.picnic_id == registration_request.picnic_id,
                         models.PicnicRegistration.user_id == registration_request.user_id)).first()
        if registration:
            return schemas.PicnicRegistration.from_orm(registration)
        registration = models.PicnicRegistration(**registration_request.dict())
        db.add(registration)
        try:
            db.commit()
        except IntegrityError:
            raise ForeignKeyIntegrityError(
                detail=f"Пикник с id={registration_request.picnic_id} "
                       f"или пользователь с id={registration_request.user_id} не найден!")

        db.refresh(registration)
        return schemas.PicnicRegistration.from_orm(registration)


class PicnicRepository(AppRepository):
    def create_picnic(self, picnic: schemas.CreatePicnicRequest):

        p = models.Picnic(**picnic.dict(by_alias=True))
        self.db.add(p)
        try:
            self.db.commit()
        except IntegrityError:
            raise ForeignKeyIntegrityError(detail=f"Город с id={picnic.city_id} не найден!")

        self.db.refresh(p)
        picnic = schemas.Picnic.from_orm(p)
        return picnic

    def get_picnics(self, picnic_filter: schemas.GetPicnicsRequest) -> List[schemas.Picnic]:
        picnics = self.db.query(models.Picnic)
        if picnic_filter.datetime is not None:
            picnics = picnics.filter(models.Picnic.time == picnic_filter.datetime)
        elif not picnic_filter.past:
            picnics = picnics.filter(models.Picnic.time >= datetime.now())
        picnics = picnics.all()
        return [schemas.Picnic.from_orm(picnic) for picnic in picnics]
