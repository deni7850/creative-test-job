import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from starlette.testclient import TestClient

from config.config import DATABASE_URL
from main import app
from src.models import Base
from src.routers import get_db
from tests import model_factory

engine = create_engine(DATABASE_URL)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture(scope='module')
def connection():
    # default_engine = create_engine(str(POSTGRES_DB_URL), isolation_level="AUTOCOMMIT")
    # with default_engine.connect() as default_conn:
    #     default_conn.execute(f"DROP DATABASE IF EXISTS {DATABASE_NAME}_test")
    #     default_conn.execute(f"CREATE DATABASE {DATABASE_NAME}_test")

    connection = engine.connect()
    Base.metadata.create_all(bind=engine)
    yield connection
    connection.close()
    Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope='function')
def session(connection):
    transaction = connection.begin()
    session = TestingSessionLocal(bind=connection)
    model_factory.CityFactory._meta.sqlalchemy_session = session
    model_factory.UserFactory._meta.sqlalchemy_session = session
    model_factory.PicnicFactory._meta.sqlalchemy_session = session
    model_factory.PicnicRegistration._meta.sqlalchemy_session = session
    yield session
    session.close()
    transaction.rollback()


@pytest.fixture
def client(session) -> TestClient:
    def _get_db_override():
        return session

    app.dependency_overrides[get_db] = _get_db_override
    return TestClient(app)
