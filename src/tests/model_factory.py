import datetime

import factory

import models


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.User

    name = factory.Sequence(lambda n: "UserName #%s" % n)
    surname = "Default Surname"
    age = 25


class CityFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.City
        sqlalchemy_get_or_create = ('name',)

    name = factory.Sequence(lambda n: "City #%s" % n)


class PicnicFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Picnic

    city = factory.SubFactory(CityFactory)
    time = datetime.datetime.now() - datetime.timedelta(seconds=10)


class PicnicRegistration(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.PicnicRegistration
        sqlalchemy_get_or_create = ('user_id', 'picnic_id')

    user = factory.SubFactory(UserFactory)
    picnic = factory.SubFactory(CityFactory)
