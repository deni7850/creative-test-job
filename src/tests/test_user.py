from typing import List

from pydantic import parse_obj_as

import models
from src import schemas
from tests import model_factory


class TestUserApi:
    def test_can_create_user(self, session, client):
        name = 'User 1'
        surname = 'Surname'
        age = 28
        user = schemas.RegisterUserRequest(
            name=name, surname=surname, age=age)

        response = client.post('user/create', json=user.dict())

        user_id = session.query(models.User).one().id
        expected_result = {
            "id": user_id,
            "name": name,
            "surname": surname,
            "age": age
        }
        assert response.status_code == 200
        assert response.json() == expected_result

    # todo: параметризовать, почему-то не находит фикстуру test_input
    # @pytest.mark.parametrize(
    #     "test_input", [{
    #             "name": 121,
    #             "surname": "Surname",
    #             "age": "25as"},
    #         {
    #             "surname": "Surname",
    #             "age": 25},
    #         {
    #             "name": "Some Name",
    #             "age": "25"
    #         }], indirect=True)
    def test_cant_create_invalid_user(self, session, client):
        r1 = {
            "name": 121,
            "surname": "Surname",
            "age": "25as"
        }
        r2 = {
            "surname": "Surname",
            "age": 25
        }
        r3 = {
            "name": "Some Name",
            "age": "25"
        }
        response = client.post('user/create', params=r1)
        assert response.status_code == 422
        response = client.post('user/create', params=r2)
        assert response.status_code == 422
        response = client.post('user/create', params=r3)
        assert response.status_code == 422

    def test_can_filter_users_by_age(self, session, client):
        # можно сделать увеличение возраста через Sequence в UserFactory
        # наверное, стоит вынести создание моделей в фикстуру, а тест разбить на кейсы
        model_factory.UserFactory.create(age=10)
        model_factory.UserFactory.create(age=15)
        model_factory.UserFactory.create(age=20)
        model_factory.UserFactory.create(age=25)
        model_factory.UserFactory.create(age=30)
        session.commit()

        response = client.get('user/get')
        users = parse_obj_as(List[schemas.UserModel], response.json())
        assert response.status_code == 200
        assert len(users) == 5

        response = client.get('user/get', params=schemas.GetUsersRequest(max_age=20))
        users = parse_obj_as(List[schemas.UserModel], response.json())
        assert response.status_code == 200
        assert len(users) == 3

        response = client.get('user/get', params=schemas.GetUsersRequest(min_age=25))
        users = parse_obj_as(List[schemas.UserModel], response.json())
        assert response.status_code == 200
        assert len(users) == 2

        response = client.get('user/get', params=schemas.GetUsersRequest(min_age=15, max_age=24))
        users = parse_obj_as(List[schemas.UserModel], response.json())
        assert response.status_code == 200
        assert len(users) == 2
