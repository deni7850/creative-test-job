import datetime
from typing import List

from pydantic import parse_obj_as

import models
from src import schemas
from tests import model_factory


class TestPicnic:
    def test_can_create_picnic(self, client, session):
        city = model_factory.CityFactory.create()
        session.commit()
        request = schemas.CreatePicnicRequest(
            city_id=city.id,
            time=datetime.datetime.now()
        )
        response = client.post('picnic/create', params=request.dict())
        assert response.status_code == 200

    def test_cant_create_picnic_without_saved_city(self, client, session):
        expected_response = {
            "detail": "Город с id=1 не найден!"
        }
        request = schemas.CreatePicnicRequest(
            city_id=1,
            time=datetime.datetime.now()
        )
        response = client.post('picnic/create', params=request.dict())
        assert response.status_code == 400
        assert response.json() == expected_response

    def test_can_find_picnics(self, client, session):
        date_for_old = datetime.datetime.now() - datetime.timedelta(days=5)
        date_for_new = datetime.datetime.now() + datetime.timedelta(days=5)
        city = model_factory.CityFactory()
        p1 = model_factory.PicnicFactory.create(city=city, time=date_for_new)
        p2 = model_factory.PicnicFactory.create(city=city)
        p3 = model_factory.PicnicFactory.create(time=date_for_old)
        session.commit()

        request = schemas.GetPicnicsRequest(datetime=p2.time)
        response = client.get('picnic/get', params=request.dict())
        picnics = parse_obj_as(List[schemas.PicnicSummaryResponse], response.json())
        assert response.status_code == 200
        assert picnics[0].id == p2.id

        response = client.get('picnic/get')
        picnics = parse_obj_as(List[schemas.PicnicSummaryResponse], response.json())
        assert response.status_code == 200
        assert len(picnics) == 3

        response = client.get('picnic/get', params=schemas.GetPicnicsRequest(past=False))
        picnics = parse_obj_as(List[schemas.PicnicSummaryResponse], response.json())
        assert response.status_code == 200
        assert len(picnics) == 1
        assert picnics[0].id == p1.id

    def test_can_register(self, client, session):
        date_for_new = datetime.datetime.now() + datetime.timedelta(days=5)
        picnic = model_factory.PicnicFactory.create(time=date_for_new)
        user = model_factory.UserFactory.create()
        session.commit()
        expected_result = {
            'user_id': user.id,
            'picnic_id': picnic.id
        }
        registration = schemas.CreatePicnicRegistrationRequest(user_id=user.id, picnic_id=picnic.id)
        response = client.post('picnic/register', json=registration.dict())
        assert response.status_code == 200
        assert response.json() == expected_result

        registrations_before = len(session.query(models.PicnicRegistration).all())
        response = client.post('picnic/register', json=registration.dict())
        registrations_after = len(session.query(models.PicnicRegistration).all())
        assert response.status_code == 200
        assert registrations_before == registrations_after

    def test_cant_register(self, client, session):
        expected_response = {
            "detail": "Пикник с id=1 или пользователь с id=1 не найден!"
        }
        request = {
            'user_id': 1,
            'picnic_id': 1
        }
        response = client.post('picnic/register', json=request)
        assert response.status_code == 400
        assert response.json() == expected_response

        picnic = model_factory.PicnicFactory.create()
        session.commit()
        response = client.post('picnic/register', json=request)
        expected_response = {
            "detail": f"Пикник с id={picnic.id} или пользователь с id=1 не найден!"
        }
        assert response.status_code == 400
        assert response.json() == expected_response
