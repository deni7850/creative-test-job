import json

import models
from src import schemas
from tests import model_factory


class TestCityApi:
    def test_can_create_city(self, client, session):
        # todo: Mock weather api
        # pytest.MonkeyPatch()
        city_name = 'Москва'

        city = schemas.CreateCityRequest(name=city_name)
        response = client.post('city/create', json=city.dict())
        city_id = session.query(models.City).filter(models.City.name == city_name).one().id
        expected_response = {
            "name": city_name,
            "id": city_id
        }
        assert response.status_code == 200
        assert response.json() == expected_response

    def test_cant_create_fake_city(self, client):
        expected_response = {
            "detail": "Параметр city должен быть существующим городом"
        }
        city = schemas.CreateCityRequest(name="city_name")
        response = client.post('city/create', json=city.dict())
        assert response.status_code == 400
        assert response.json() == expected_response

    def test_cant_duplicate_cities(self, session, client):
        city_name = 'Москва'
        city = schemas.CreateCityRequest(name=city_name)
        response = client.post('city/create', json=city.dict())

        city_id = session.query(models.City).filter(models.City.name == city_name).one().id
        expected_response = {
            "name": city_name,
            "id": city_id
        }
        assert response.status_code == 200
        assert response.json() == expected_response
        response = client.post('city/create', json=city.dict())
        assert response.status_code == 200
        assert response.json() == expected_response

    def test_can_find_cities(self, session, client):
        city = model_factory.CityFactory(name="Тюмень")
        city2 = model_factory.CityFactory.create(name="Москва")
        session.commit()

        expected_response = [
            {
                "name": city.name,
                "id": city.id
            },
            {
                "name": city2.name,
                "id": city2.id
            }]
        response = client.get('city/get')
        assert response.status_code == 200
        assert response.json() == expected_response
        expected_response = [
            {
                "name": city.name,
                "id": city.id
            }
        ]
        response = client.get('city/get', params={'name': "%юме%"})
        assert response.status_code == 200
        assert response.json() == expected_response

    def check_no_city_found(self, client):
        expected_response = json.dumps([])
        response = client.get('city/get')
        assert response.status_code == 200
        assert response == expected_response

    def test_can_get_weather(self, client, session):
        city = model_factory.CityFactory()
        # todo: сравнивать c результатами Мока
        response = client.get('city/get-weather', params={'name': city.name})
        assert response.status_code == 200
