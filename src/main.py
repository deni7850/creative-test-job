import uvicorn
from fastapi import FastAPI

from src.routers import picnic, city, user

app = FastAPI()
app.include_router(picnic.router)
app.include_router(city.router)
app.include_router(user.router)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
